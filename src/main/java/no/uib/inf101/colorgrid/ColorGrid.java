package no.uib.inf101.colorgrid;

import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class ColorGrid implements IColorGrid{
  private int rows;
  private int cols;
  private List<CellColor> cells;
  private List<List<Color>> grid;

  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<>();
    for (int i = 0; i < rows; i++) {
      List<Color> cells = new ArrayList<>();
      for (int j = 0; j < cols; j++) {
        cells.add(null);
        
      }
      grid.add(cells);
    }
  }
  public Color get(CellPosition pos){
    int col = pos.col();
    int row = pos.row();
    return grid.get(row).get(col);


  }
  public void set(CellPosition pos, Color color){
    int row = pos.row();
    int col = pos.col();
    grid.get(row).set(col, color);
  }
  public int rows(){
    return rows;
  }
  public int cols(){
    return cols;

  }
  public List<CellColor> getCells(){
    List<CellColor> cellColorList = new ArrayList<>();
    for (int i = 0; i < grid.size(); i++) {
      for (int j = 0; j < grid.get(0).size(); j++) {
        CellPosition pos = new CellPosition(i, j);
        Color color = grid.get(i).get(j);
        CellColor cellColor = new CellColor(pos, color);
        cellColorList.add(cellColor);
  
        
      }
      
    }
    return cellColorList;
  }
}
