package no.uib.inf101.gridview;

import javax.swing.JFrame;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import java.awt.Color;

public class Main {
  public static void main(String[] args) {
    ColorGrid colorGrid = new ColorGrid(3, 4);
    GridView gridView = new GridView(colorGrid);
    JFrame frame = new JFrame();
    frame.setContentPane(gridView);
    frame.setTitle("INF101");
    frame.setContentPane(gridView);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

    // Opprett Colorview objekt 3 rows 4 cols
    
    
    // Sett farger
    colorGrid.set(new CellPosition(0, 0), Color.RED);
    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
    colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
    colorGrid.set(new CellPosition(2, 3), Color.GREEN);
  }
}
