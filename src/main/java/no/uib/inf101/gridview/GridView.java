package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.List;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;

public class GridView extends JPanel {
  IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid){
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;

  }
  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    
    drawGrid(g2);
  }
  private void drawGrid(Graphics2D graphics2d){
    CellPositionToPixelConverter cellPositionToPixelConverter = new CellPositionToPixelConverter(getBounds(), colorGrid, OUTERMARGIN);
    drawCells(graphics2d, colorGrid, cellPositionToPixelConverter);
    
  }
  private static void drawCells(Graphics2D graphics2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter){
    List<CellColor> cells = cellColorCollection.getCells();

    for (int i = 0; i < cells.size(); i++) {
      CellColor cell = cells.get(i);
      if (cell.color() == null) {
        graphics2d.setColor(Color.DARK_GRAY);
      }else{
        graphics2d.setColor(cell.color());
      }
    

    CellPosition cellPosition = cell.cellPosition();
    Rectangle2D cellBounds = cellPositionToPixelConverter.getBoundsForCell(cellPosition);
    graphics2d.fill(cellBounds);
}
}
  }
  
  



